# 1. (0.05 pkt) Powiększony nagłówek 
2. (0.05 pkt) Co najmniej trzy paragrafy
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis massa tempus dolor condimentum laoreet et vitae est. Nunc eu est imperdiet, interdum ipsum nec, hendrerit ex. Donec sed semper augue. Duis vehicula sapien in dolor egestas, eget cursus dui vestibulum. Nam suscipit, justo in rhoncus sodales, eros sem varius lorem, eget dictum dolor elit et sapien. Etiam ullamcorper nec dolor sit amet vulputate. Donec vel purus et neque vehicula accumsan eu vitae ligula. Nam nec finibus libero. Mauris congue eget risus tristique laoreet. Praesent accumsan ut metus at sollicitudin. Sed eu sapien est. Proin consequat quam quis tellus fringilla, vitae tincidunt magna rutrum. Suspendisse posuere ultrices lorem, et varius libero euismod a.
Nunc quis hendrerit lacus, sed placerat felis. Nullam sit amet suscipit orci. Ut et ullamcorper leo, eu lobortis arcu. Morbi nec ex eget sem mattis porttitor. Maecenas consequat fermentum justo, quis tincidunt magna. Duis consequat volutpat pretium. Nullam consectetur tempor arcu. Vivamus nec lobortis lorem. Ut nec urna tortor. Donec nec bibendum lacus. Morbi ac faucibus diam. Pellentesque vestibulum porttitor ornare. Sed commodo eu odio id pretium. Integer vel sapien sollicitudin, tristique ipsum ac, egestas sem. Pellentesque non dolor nunc. Proin at elit at sapien consectetur egestas at vitae est.
Morbi ante justo, maximus id fermentum vel, aliquam ac eros. Nullam blandit consequat lorem, ac posuere tellus viverra sit amet. Praesent nisi ex, mollis at turpis eu, laoreet tincidunt quam. Donec volutpat metus sapien, nec commodo lorem pharetra sit amet. Nunc sem diam, volutpat in magna ut, accumsan commodo lectus. Nullam ac finibus justo, ac varius lacus. In pellentesque facilisis rhoncus. Cras venenatis commodo volutpat. Vivamus sollicitudin suscipit finibus. Cras tellus tellus, posuere at nibh vel, interdum molestie lectus. 
3. (0.05 pkt) Co najmniej jedno przekreślenie, pogrubienie oraz kursywa
**Lorem ipsum** dolor *sit* amet, ~~consectetur adipiscing elit~~.
4. (0.05 pkt) Co najmniej jeden cytat
>– To wódka? – słabym głosem zapytała Małgorzata.(...)
– Na litość boską, królowo – zachrypiał – czy ośmieliłbym się nalać damie wódki? To czysty spirytus. 
5. (0.1 pkt) Zagnieżdżoną listę numeryczną
    1. Działa
    2. Prawda?
    3. Prawda
6. (0.1 pkt) Zagnieżdżoną listę nienumeryczną
    * nieważne czy gwiazdka
    - czy myślnik
    + czy plus
        + ważna jest indentacja
7. (0.05 pkt) Blok kodu programu, który ma co najmniej 3 linie
```py
import this
print("Podnobno printy to nie program")
print("Ale nie to dzisiaj pytajo")
```
8. (0.05 pkt) Kod programu zagnieżdżony w tekście
Pellentesque a iaculis ipsum, eu placerat `ipsum`. Aenean fringilla leo sed mi  ``pellentesque``, sed auctor libero aliquam.
9. (0.1 pkt) Dowolny obraz, który będzie ładowany z repozytorium
![Pictures/Matematyka_elementarna_be_likepsifi.jpg](Pictures/Matematyka_elementarna_be_likepsifi.jpg)
